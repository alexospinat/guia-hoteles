$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.popover-dismiss').popover({
        trigger: 'focus'
    });
    $('.carousel').carousel({
        interval: 2000
    });

    $("#contacto").on('show.bs.modal', function (e) {
        console.log("El query se esta ejecutando");
        $("#btncontacto").removeClass("btn-primary");
        $("#btncontacto").addClass("btn-outline.success");
        $("#btncontacto").prop('disabled', true);
    })
    $("#contacto").on('shown.bs.modal', function (e) {
        console.log("El query se ejecut�");
    })
    $("#contacto").on('hide.bs.modal', function (e) {
        console.log("El query se esta ocultando");
    })
    $("#contacto").on('hidden.bs.modal', function (e) {
        console.log("El query se acult�");
        $("#btncontacto").prop('disabled', false);
        $("#btncontacto").removeClass("btn-outline.success");
        $("#btncontacto").addClass("btn-primary");
    })

});